const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3000;

app.use(cors({
  credentials: true,
}));

app.listen(port,function () {
  console.log('CORS-enabled web server listening on port 3000');
});

//console.log("API escuchando en el puerto " + port);
const bodyParser= require('body-parser');
app.use(bodyParser.json());

const userController = require('./controllers/userControllers');
const sessionController = require('./controllers/sessionControllers');
const accountController = require('./controllers/accountControllers');
const transactionController = require('./controllers/transactionControllers');
const locationController = require('./controllers/locationControllers');
const pictureController = require('./controllers/pictureControllers');

app.post('/apitechu/v1/users', userController.createUserV1);
app.get('/apitechu/v1/users/:id', userController.getUserV1);
app.post('/apitechu/v1/users/:id/sessions', sessionController.createSessionV1);
app.delete('/apitechu/v1/users/:id1/sessions/:id2', sessionController.deleteSessionV1);
app.get('/apitechu/v1/users/:id/accounts', accountController.getUserAccountsV1);
app.get('/apitechu/v1/users/:id1/accounts/:id2/transactions',transactionController.getAccountTransactionsV1);
app.post('/apitechu/v1/users/:id1/accounts/:id2/transactions',transactionController.createAccountTransactionsV1);
app.post('/apitechu/v1/users/:id/pictures', pictureController.createUserPictureProfileV1);

app.get('/apitechu/v1/provinces/', locationController.getProvincesV1);
app.get('/apitechu/v1/provinces/:id', locationController.getProvinceByIdV1);
app.get('/apitechu/v1/provinces/:id/municipalities', locationController.getMunicipalitiesV1);
app.get('/apitechu/v1/provinces/:id/forecast', locationController.getProvinceForecastV1);
app.get('/apitechu/v1/provinces/:provinceId/municipalities/:municipalityId', locationController.getMunicipalityByIdV1);


app.post('/apitechu/v1/tokens',pictureController.createTokenV1);
