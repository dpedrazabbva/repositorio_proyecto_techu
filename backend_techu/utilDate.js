const moment = require('moment');

function nextValuableDate()
{

  var fechaActual = moment();

  if (fechaActual.day()==0){
    return fechaActual.add(1, 'days').toDate();
  }
  else {
    if (fechaActual.day()==6){
      return fechaActual.add(2, 'days').toDate();
    }
    else{
      return fechaActual.toDate();
    }
  }
}

module.exports.nextValuableDate = nextValuableDate;
