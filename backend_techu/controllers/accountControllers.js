const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudpr6ed/collections/";
const mLabAPIKey = "apiKey=5ErqEthZdY9RSLgg2J3vjNY7W34SM96c";
const requestJson = require('request-json');
const validateToken = require('../validateToken');
const random = require("node-random");

function getUserAccountsV1(req,res){
  var userId = req.params.id;
  var filter = req.query.number;

  if(filter==null){
    var query = 'q={"participants.id" : "' + userId +'"}';
  }else{
    var query = 'q={"number" : "' + filter +'"}';
  }

  console.log("getUserAccountsV1 - El filtro es " + query);
  var token = req.headers['authorization'];

  console.log("getUserAccountsV1 - GET /apitechu/v1/users/" + userId + "/accounts");
  console.log ("getUserAccountsV1 - Recuperando las cuentas del usuario: " + userId);
  console.log("getUserAccountsV1 - El filtro es " + query);

  if(validateToken.validateToken(token)){
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("getUserAccountsV1 - Ciente HTTP para mLab creado");

    httpClient.get("account?" + mLabAPIKey + "&" + query,
      function (err, resMLab, body){
        if(resMLab.statusCode==200){
          if(body.length > 0){
            console.log("getUserAccountsV1 - Se ha recuperado la información de las cuentas del usuario " + userId);
            let respuesta = body;
            res.send(respuesta);
          }
          else{
            console.log("getUserAccountsV1 - El usuario " + userId + " no tiene cuentas");
            let respuesta = {"msg" : "El usuario no tiene cuentas"};
            res.status(404);
            res.send(respuesta);
          }
        }
        else {
          console.log("getUserAccountByIdV1 - Se ha producido un error recuperando la información de las cuentas del usuario " + userId);
          res.status(500);
          let respuesta = {"msg" : "No se han podido recuperar las cuentas del usuario"};
          res.send(respuesta);
        }
      }
    );
  }
  else{
    let respuesta = {"msg" : "Token inválido"};
    res.status(401);
    res.send(respuesta);
  }
}

function createUserInitialAccount(user){
  console.log("createUserInitialAccount - Vamos a crear una cuenta inicial al usuario " + user);
  var newNumberAccount="12345678901234567890";

  calculateNewAccount().then(function(newNumberAccount){
    console.log("createUserInitialAccount - Se ha creado el número de cuenta " + newNumberAccount);

    var newAccount = {
      "number" : newNumberAccount,
      "currency" : "EUR",
      "type" : "CC",
      "alias" : "Cuenta Corriente inicial",
      "balance" : 0,
      "participants" : [{"id" : user , "relationship" : "TIT"}]
    }

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("createUserInitialAccount - Ciente HTTP para mLab creado");
    httpClient.post("account?" + mLabAPIKey, newAccount,
      function (err, resMLab, body){
        console.log("createUserInitialAccount - Cuenta inicial " + body.number + " creada para el usuario " + user);
      }
    );
  },function(error){
    console.log("createUserInitialAccount - Se ha producido un error al generar el nuevo número de cuenta");
  });
}

function validateAccount(number,userId){
  console.log("validateAccount - Validamos los datos de la cuenta " + number + " ...");
  var query = 'q={"number" : "' + number + '"}';

  var promesa = new Promise(function(resolve,reject){
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("validateAccount - Cliente HTTP para mLab creado");
    httpClient.get("account?" + mLabAPIKey + "&" + query,
      function (err, resMLab, body){
        if(err){
          console.log("validateAccount - Se ha producido un error validando los datos de la cuenta " + number);
          reject(err);
        }
        else {
          if(resMLab.statusCode==200){
            if(body.length>0){
              console.log("validateAccount - Se han validado correctamente los datos de la cuenta");
              resolve(body[0]);
            }
            else {
              console.log("validateAccount - La cuenta " + number + " no existe");
              reject({"respuesta" : {"msg" : "La cuenta no existe"},
                      "statusCode" : 404});
            }
          }
          else{
            console.log("validateAccount - No se encuentra información de la cuenta");
            reject({"respuesta" : {"msg" : "No se puede validar la información de la cuenta"},
                    "statusCode" : 404});
          }
        }
      }
    );
  });

  return promesa;
}

function calculateNewAccount(){
  console.log("calculateNewAccount - Calculando IBAN de la nueva cuenta...");

  var promesa = new Promise(function(resolve,reject){
    var newAccount = "newAccount";
    random.strings({
        "length": 11,
        "number": 2,
        "upper": false,
        "lower": false,
        "unique": true
    }, function(error, data) {
       if (error){
         reject(error);
       }else {
         newAccount = "ES" + data[0] + data[1];
         console.log("calculateNewAccount - " + newAccount);
         resolve(newAccount);
       }
    });
  });

  return promesa;
}

function updateAccountBalance(number,balance,balance_currency,amount,amount_currency){
  console.log("updateAccountBalance - Actualizamos el saldo de " + number + " ...");
  console.log("balance_currency = " + balance_currency);
  console.log("amount_currency = " + amount_currency);

  var query = 'q={"number" : "' + number + '"}';

  var promesa = new Promise(function(resolve,reject){
    if(balance_currency==amount_currency){
      var new_balance = Number(balance) + Number(amount);
    }
    else {
      console.log("La divisa del movimiento y de la cuenta no coinciden. No se puede actualizar el saldo.")
      reject({"respuesta" : {"msg" : "La divisa del movimiento y de la cuenta no coinciden. No se puede actualizar el saldo"},
              "statusCode" : 500})
    }

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("updateAccountBalance - Cliente HTTP para mLab creado");
    var putBody = '{"$set":{"balance":' + new_balance + '}}';
    httpClient.put("account?" + mLabAPIKey + "&" + query, JSON.parse(putBody),
      function (err, resMLab, body){
        if(err){
          console.log("updateAccountBalance - Se ha producido un error actualizando el saldo de la cuenta " + number);
          reject({"respuesta" : {"msg" : "Se ha producido un error actualizando el saldo de la cuenta"},
                  "statusCode" : 500});
        }
        else {
          if(resMLab.statusCode==200){
              console.log("updateAccountBalance - Se ha actualizado el saldo de la cuenta " + number);
              resolve({"respuesta" : {"msg" : "Se ha actualizado el saldo de la cuenta"},
                      "statusCode" : 201});
            }
            else {
              console.log("updateAccountBalance - La cuenta " + number + " no existe");
              reject({"respuesta" : {"msg" : "La cuenta no existe"},
                      "statusCode" : 404});
            }
        }
      }
    );
  });

  return promesa;
}

module.exports.getUserAccountsV1 = getUserAccountsV1;
module.exports.createUserInitialAccount = createUserInitialAccount;
module.exports.validateAccount = validateAccount;
module.exports.updateAccountBalance = updateAccountBalance;
