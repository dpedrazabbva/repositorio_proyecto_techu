const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudpr6ed/collections/";
const mLabAPIKey = "apiKey=5ErqEthZdY9RSLgg2J3vjNY7W34SM96c";
const requestJson = require('request-json');
const userController = require('./userControllers');
const jwt = require('jsonwebtoken');
const clavePublica ="Clave Pública";

function createSessionV1(req,res){
  var userId = req.params.id;
  var password = req.body.password;
  var user={};
  var resultado;
  var respuesta = {"msg" : "No se puede crear sesión para el usuario " + userId + " porque no existe"};

  console.log("POST /apitechu/v1/users/" + userId + "/sessions");
  console.log ("Iniciando sesión para el usuario: " + userId + " con password: " + password);
  if(password==null){
    res.status(400);
    respuesta={"msg" : "Falta la password"};
    res.send(respuesta);
  }
  else {
    userController.getUserById(userId).then(function(user) {
      if (user!=null){
        console.log("Se ha recuperado la información del usuario " + user.id);
        if (crypt.checkPassword(password,user.password)){
          console.log("Clave de usuario verificada correctamente");

          userController.updateUserLogged(user.id,true).then(function(resultado){

            if(resultado){
              var newSession = {
                "user_id" : userId,
                "status" : "V",
                "opening_date" : new Date()
              };

              createSessionRequest(newSession).then(function(respuesta){
                console.log("La respuesta a la creación de sesión es " + JSON.stringify(respuesta));
                res.status(201);
                res.send(respuesta);
              }, function (error){
                  console.log("Se ha producido un error al crear la sesión: " + error);
                  res.send(error);
                }
              );
            }
            else {
              respuesta={"msg":"Se ha producido un error actualizando la información del inicio de sesión"};
              res.send(respuesta);
            }

          }
          , function(error){
              console.log("Se ha producido un error en updateUserLogged");
          });
        }
        else {
          respuesta={"msg":"La clave del usuario es incorrecta"};
          res.status(401);
          res.send(respuesta);
        }
      }
      else {
        console.log("No se puede crear sesión para el usuario " + userId + " porque no existe");
        respuesta = {"msg" : "No se puede crear sesión para el usuario " + userId + " porque no existe"};
        res.status(500);
        res.send(respuesta);
      }

    }, function(respuesta) {
      console.log("Se ha producido un error recuperando la información del usuario. ", error);
      respuesta = {"msg" : "Se ha producido un error recuperando la información del usuario"};
      res.send(respuesta);
    });
  }
}

function deleteSessionV1(req,res){
  var userId = req.params.id1;
  var sessionId = req.params.id2;

  console.log("DELETE /apitechu/v1/users/" + userId + "/sessions/" +sessionId);

  validateSession(sessionId,userId).then(function(resultado){
    if(resultado){
      deleteSessionRequest(sessionId).then(function(resultado){
        if(resultado){
          userController.updateUserLogged(userId,false).then(function(resultado){
            if(resultado){
              let respuesta = {"msg" : "Se ha eliminado la sesión"};
              res.status(204);
              res.send(respuesta);
            }
            else {
              let respuesta={"msg":"Se ha producido un error actualizando la información del cierre de sesión"};
              res.status(500);
              res.send(respuesta);
            }
          } , function(resultado){
                console.log("Se ha producido un error en updateUserLogged");
                let respuesta={"msg" : "Se ha borrado la sesión pero no se ha podido actualizar la información de usuario"};
                res.status(500);
                res.send(respuesta);
              });
        }
        else {
          let respuesta = {"msg" : "Se ha producido un error borrando la sesión"};
          res.status(500);
          res.send(respuesta);
        }
      }, function(error){
        let respuesta = {"msg" : "Se ha producido un error al borrar la sesión"};
        res.status(500);
        res.send(respuesta);
      });
    }
    else {
      let respuesta = {"msg" : "Los datos de la sesión y/o del usuario no son correctos"};
      res.status(400);
      res.send(respuesta);
    }
  }, function(error){
    let respuesta = {"msg" : "Se ha producido un error validando la sesión"};
    res.status(500);
    res.send(respuesta);
  });
}

function createSessionRequest (newSession){
  console.log("En la función de espera...");

  var promesa = new Promise(function(resolve,reject){
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP para mLab creado");
    httpClient.post("session?" + mLabAPIKey, newSession,
      function (err, resMLab, body){
        if(err){
          let respuesta = {"msg" : "Se ha producido un error creando la sesión del usuario " + body.user_id};
          reject(respuesta);
        }
        else {
          console.log("Se ha creado la sesión " + body._id.$oid + " para el usuario " + body.user_id);
          var token = jwt.sign(newSession, clavePublica, {expiresIn: 60 * 60 * 24});
          let respuesta = {"id":body._id.$oid,
                        "token" : token,
                        "msg" : "Sesión creada con éxito"};
          resolve(respuesta);
        }
      }
    );
  });

  return promesa;
}

function deleteSessionRequest (session){
  console.log("En la función deleteSessionRequest...");
  var query = "session/" + session + "?"+ mLabAPIKey;
  var putBody = {"$set":{"status":"C","closing_date": new Date()}};

  var promesa = new Promise(function(resolve,reject){
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP para mLab creado");
    httpClient.put(query, putBody,
      function (err, resMLab, body){
        if(err){
          console.log("Se ha producido un error eliminando la sesión " + session);
          resolve(false);
        }
        else {
          console.log("Se ha eliminado la sesión " + body._id.$oid + " para el usuario " + body.user_id);
          resolve(true);
        }
      }
    );
  });

  return promesa;
}

function validateSession(sessionId,userId){
  console.log("Validamos la sesión " + sessionId + " ...");

  var promesa = new Promise(function(resolve,reject){
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP para mLab creado");
    httpClient.get("session/" + sessionId + "?" + mLabAPIKey,
      function (err, resMLab, body){
        if(err){
          console.log("Se ha producido un error validando los datos de la sesión " + sessionId);
          resolve(false);
        }
        else {
          console.log(resMLab.statusCode);
          if(resMLab.statusCode==200){
            console.log("Se ha recuperado la información de la sesión " + sessionId);
            if(body!=null){
              if(body.user_id==userId){
                console.log("Se han validado correctamente los datos de sesión y usuario");
                resolve(true);
              }
              else {
                console.log("El usuario al que pertenece la sesión no corresponde con el de la petición");
                resolve(false);
              }
            }
            else {
              console.log("No se puede validar la información del usuario de la sesión");
              resolve(false);
            }
          }
          else{
            console.log("La sesión " + sessionId + " no existe");
            resolve(false);
          }
        }
      }
    );
  });

  return promesa;

}

module.exports.createSessionV1 = createSessionV1;
module.exports.deleteSessionV1 = deleteSessionV1;
