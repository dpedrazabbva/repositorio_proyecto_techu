const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudpr6ed/collections/";
const mLabAPIKey = "apiKey=5ErqEthZdY9RSLgg2J3vjNY7W34SM96c";
const baseAemetURL = "https://opendata.aemet.es/opendata/api/";
const aemetAPIKey = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwcm95ZWN0by50ZWNodS5kcHJAZ21haWwuY29tIiwianRpIjoiOGFjZWU1MDEtZjZkMS00YmUyLWEyZWItOTEwNTlkMmNhOWYwIiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE1MzUzNTM0NzQsInVzZXJJZCI6IjhhY2VlNTAxLWY2ZDEtNGJlMi1hMmViLTkxMDU5ZDJjYTlmMCIsInJvbGUiOiIifQ.LTGdtEYyGUqKViHeYbM7PdEYZSNbA8674KkTcAe3HhI";
const requestJson = require('request-json');

function getProvincesV1 (req,res){
  console.log("getProvincesV1 - Recuperando el listado de las provincias...");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("getProvincesV1 - Cliente HTTP para mLab creado");

  httpClient.get("province?" + mLabAPIKey,
    function (err, resMLab, body){
      if(err){
        console.log("getProvincesV1 - Error recuperadno la lista de provincias ");
        res.status(500);
        res.send({"msg":"Se ha producido un error al recuperar la lista de provincias"});
      }
      else {
        console.log("getProvincesV1 - Se ha recuperado la lista de provincias");
        res.send(body);
      }
    }
  );
}

function getProvinceByIdV1 (req,res){
  var provinceId = req.params.id;
  var query = 'q={"cod_provincia" : ' + provinceId +'}';
  console.log("getProvinceByIdV1 - Recuperando la provincia con id = " + provinceId);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("getProvinceByIdV1 - Cliente HTTP para mLab creado");

  httpClient.get("province?" + mLabAPIKey + "&" + query,
    function (err, resMLab, body){
      if(err){
        console.log("getProvinceByIdV1 - Error recuperadno la provincia con id " + provinceId);
        res.status(500);
        res.send({"msg":"Se ha producido un error al recuperar la provincia"});
      }
      else {
        console.log("getProvinceByIdV1 - Se ha recuperado la información de la provincia " + provinceId);
        res.send(body);
      }
    }
  );
}

function getMunicipalitiesV1(req,res){
  var provinceId = req.params.id;
  var query = 'q={"cod_provincia" : ' + provinceId +'}';
  console.log("getMunicipalitiesV1 - Recuperando el listado de localidades de la provincia " + provinceId + " ...");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("getMunicipalitiesV1 - Cliente HTTP para mLab creado");
  httpClient.get("municipality?" + mLabAPIKey + "&" + query,
    function (err, resMLab, body){
      if(resMLab.statusCode==200){
        if(body.length > 0){
          console.log("getMunicipalitiesV1 - Se ha recuperado las localidades de la provincia " + provinceId);
          let respuesta = body;
          res.send(respuesta);
        }
        else{
          console.log("getMunicipalitiesV1 - La provinca " + provinceId + " no tiene localidades");
          let respuesta = {"msg" : "La provincia no tiene localidades"};
          res.status(404);
          res.send(respuesta);
        }
      }
      else {
        console.log(err);
        console.log("getMunicipalitiesV1 - Se ha producido un error recuperando la información de las localidades de la provincia " + provinceId);
        res.status(500);
        let respuesta = {"msg" : "No se han podido recuperar las localidades de la provincia"};
        res.send(respuesta);
      }
    }
  );
}

function getMunicipalityByIdV1 (req,res){
  var municipalityId = req.params.municipalityId;
  var provinceId = req.params.provinceId;
  var query = 'q={"cod_provincia" : ' + provinceId + ',"cod_municipio" : ' + municipalityId + '}';
  console.log("getMunicipalityByIdV1 - Recuperando el municipio " + municipalityId + "de la provincia " + provinceId);
  console.log("La query es " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("getMunicipalityByIdV1 - Cliente HTTP para mLab creado");

  httpClient.get("municipality?" + mLabAPIKey + "&" + query,
    function (err, resMLab, body){
      if(err){
        console.log("getMunicipalityByIdV1 - Error recuperadno el municipio con id " + municipalityId);
        res.status(500);
        res.send({"msg":"Se ha producido un error al recuperar el municipio"});
      }
      else {
        console.log("getMunicipalityByIdV1 - Se ha recuperado la información del municipio " + municipalityId);
        res.send(body);
      }
    }
  );
}

function getProvinceForecastV1 (req,res){
  var provinceId = req.params.id;
  console.log("getProvinceForecastV1 - Recuperando el pronóstico del tiempo para la provincia " + provinceId + "...");

  var httpClient = requestJson.createClient(baseAemetURL+provinceId);
  console.log("getProvinceForecastV1 - Cliente HTTP para AEMET creado");
  httpClient.headers['api_key'] = aemetAPIKey;
  httpClient.get("prediccion/provincia/hoy/"+provinceId,
    function (err, resMLab, body){
      if(err){
        console.log("getProvinceForecastV1 - Error recuperando la predicción del tiempo ");
        res.status(500);
        res.send({"msg":"Se ha producido un error al recuperar la información del tiempo"});
      }
      else {
        console.log("getProvincesV1 - Se ha recuperado la información del tiempo");
        res.send({"url_prediccion" : body.datos});
      }
    }
  );
}

module.exports.getProvincesV1 = getProvincesV1;
module.exports.getProvinceByIdV1 = getProvinceByIdV1;
module.exports.getMunicipalitiesV1 = getMunicipalitiesV1;
module.exports.getMunicipalityByIdV1 = getMunicipalityByIdV1;
module.exports.getProvinceForecastV1 = getProvinceForecastV1;
