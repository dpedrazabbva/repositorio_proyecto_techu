const jwt = require('jsonwebtoken');
const key = require('../keys/Proyecto-Techu-59857cf92bdc.json');
const multiparty = require('multiparty');
function uploadPicture (picture){
  var token = getDriveToken();
}

function getDriveToken(){
  console.log ("Generando token JWT para conectar a Google Drive...")
  var iat = Math.round(Date.now() / 1000);
  var exp = iat + (60 * 60);

  var claim_set = {
    "iss":"proyecto-techu@sublime-wavelet-214409.iam.gserviceaccount.com",
    "scope":"https://www.googleapis.com/auth/drive.file",
    "aud":"https://www.googleapis.com/oauth2/v4/token",
    "exp":exp,
    "iat":iat
  }

  return token = jwt.sign(claim_set,key.private_key,{ header: {"alg":"RS256","typ":"JWT"}, algorithm: 'RS256'});
}

function createTokenV1(req,res){
  console.log("createTokenV1 - Creando token de Google Drive API...");
  var token = getDriveToken();
  if (token!=null){
    console.log("createTokenV1 - Token generado: " + token);
    var respuesta = {"msg":"Token generado correctamente","token" : token};
    res.status(201);
  }else{
    console.log("createTokenV1 - Error generando el token");
    var respuesta = {"msg":"Error al generar el token"};
    res.status(500);
  }

  res.send(respuesta);
}

function createUserPictureProfileV1 (req,res){
  var userId = req.params.id;

  console.log("createUserPictureProfileV1 - POST /apitechu/v1/users/" + userId + "/pictures");
/*  var jsonString = '';
  req.on('data', function (data) {
    console.log("data : " + data);
     jsonString += data;
 });
 console.log(jsonString);*/

 var form = new multiparty.Form();
 form.parse(req);
 console.log(form);

  var respuesta = {"msg":"Foto de perfil subida correctamente"};
  res.send(respuesta);
}

module.exports.uploadPicture = uploadPicture;
module.exports.createTokenV1 = createTokenV1;
module.exports.createUserPictureProfileV1 = createUserPictureProfileV1;
