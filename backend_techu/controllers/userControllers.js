const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudpr6ed/collections/";
const mLabAPIKey = "apiKey=5ErqEthZdY9RSLgg2J3vjNY7W34SM96c";
const requestJson = require('request-json');
const accountController = require('./accountControllers');
const validateToken = require('../validateToken');
const pictureController = require('./pictureControllers');

function createUserV1(req,res){
  console.log("createUserV1 - POST /apitechu/v1/users");
  var emailB64 = new Buffer(req.body.email);

  var newUser = {
    "id": emailB64.toString('base64'),
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "address" : req.body.address,
    "postal_code" : req.body.postalCode,
    "province" : req.body.province,
    "municipality" : req.body.municipality,
    "password" : crypt.hash(req.body.password),
    "logged" : false,
    "profile_picture" : req.body.profile_picture
  };

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("createUserV1 - Cliente HTTP para mLab creado");
  httpClient.post("user?" + mLabAPIKey, newUser,
    function (err, resMLab, body){
      if(resMLab.statusCode >= 400){
        console.log ("createUserV1 - Error creando el usuario: " + body.message);
        var respuesta = {"msg" : "Se ha producido un error al crear el usuario"};
        res.status(500);
      }else{
            accountController.createUserInitialAccount(body.id);
            pictureController.uploadPicture(req.body.profile_picture);
            console.log("createUserV1 - Usuario con id " + body.id + " creado con éxito");
            var respuesta = {"id":body.id,
                            "msg" : "Usuario creado con éxito"};
            res.status(201);
      }
      res.send(respuesta);
    }
  );
}

function getUserV1 (req,res){
  var userId = req.params.id;
  var token = req.headers['authorization'];

  console.log("getUserV1 - GET /apitechu/v1/users/" + userId);

  if(validateToken.validateToken(token)){
    getUserById(userId).then(function(user) {
      if(user!=null){
        console.log("getUserV1 - Se ha recuperado la información del usuario " + userId);
        res.send(user);
      }
      else{
        console.log("getUserV1 - No se ha encontrado al usuario " + userId);
        let respuesta = {"msg" : "Usuario inexistente"};
        res.status(404);
        res.send(respuesta);
      }
    }, function(error){
        console.log("getUserV1 - Se ha producido un error recuperando los datos del usuario " + userId);
        let respuesta = {"msg" : "Se ha producido un error recuperando los datos del usuario"};
        res.status(500);
        res.send(respuesta);
    });
  }
  else {
    let respuesta = {"msg" : "Token inválido"};
    res.status(401);
    res.send(respuesta);
  }
}

function getUserById (user){

  var promesa = new Promise(function(resolve,reject){

    console.log("getUserById - Recuperando información del usuario " + user);

    var httpClient = requestJson.createClient(baseMLabURL);
    var query = 'q={"id" : "' + user + '"}';
    console.log("getUserById - Cliente HTTP para mLab creado");

    httpClient.get("user?" + mLabAPIKey + "&" + query,
      function (err, resMLab, body){
        if(err){
          console.log ("getUserById - Se ha producido un error recuperando la información del usuario");
          reject(err);
        } else {
            if (body.length > 0){
              console.log("getUserById - Se ha recuperado la información del usuario " + JSON.stringify(body[0]));
              resolve(body[0]);
            }
            else{
              console.log("getUserById - No se ha encontrado al usuario " + user);
              resolve(null);
            }
        }
      }
    );
  });

  return promesa;
}

function updateUserLogged(userId,logged){
  console.log("updateUserLogged - Actualizando atributo logged = " + logged + " del usuario " + userId);
  var query = 'user?q={"id" : "' + userId + '"}&'+ mLabAPIKey;
  var putBody = {"$set":{"logged": logged }};

  var promesa = new Promise(function(resolve,reject){
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("updateUserLogged - Cliente HTTP para mLab creado");

    httpClient.put(query,putBody,
      function (err, resMLab, body){
        if(err){
          console.log("updateUserLogged - Error actualizando el atributo logged a " + logged);
          resolve(false);
        }
        else {
          console.log("updateUserLogged - Atributo logged actualizado a " + logged);
          resolve(true);
        }
      }
    );
  });

  return promesa;
}

module.exports.createUserV1 = createUserV1;
module.exports.getUserV1 = getUserV1;
module.exports.getUserById = getUserById;
module.exports.updateUserLogged = updateUserLogged;
