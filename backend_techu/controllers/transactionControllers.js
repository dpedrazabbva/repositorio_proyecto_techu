const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudpr6ed/collections/";
const mLabAPIKey = "apiKey=5ErqEthZdY9RSLgg2J3vjNY7W34SM96c";
const requestJson = require('request-json');
const accountController = require('./accountControllers');
const utilDate = require('../utilDate');
const validateToken = require('../validateToken');


function getAccountTransactionsV1(req,res){
  var userId = req.params.id1;
  var accountNumber = req.params.id2;
  var token = req.headers['authorization'];

  console.log("getAccountTransactionsV1 - GET /apitechu/v1/users/" + userId + "/accounts/" + accountNumber + "/transactions");
  console.log ("getAccountTransactionsV1 - Recuperando los movimientos de la cuenta " + accountNumber + " del usuario " + userId);

  if(validateToken.validateToken(token)){
    accountController.validateAccount(accountNumber,userId).then(function(account){
      let encontrado = false;
      for (participant of account.participants){
        if (participant.id == userId){
          encontrado = true;
          break;
        }
      }

      if (encontrado){
        console.log("getAccountTransactionsV1 - Se han validado correctamente los datos de la petición de consulta del movimiento");

        let query = 'q={"account_number" : "' + accountNumber + '"}&s={"operation_date":-1}';
        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("getAccountTransactionsV1 - Cliente HTTP para mLab creado");
        httpClient.get("transaction?" + mLabAPIKey + "&" + query,
          function (err, resMLab, body){
            if(resMLab.statusCode==200){
              if(body.length > 0){
                let respuesta = body;
                res.send(respuesta);
              }
              else{
                console.log("getAccountTransactionsV1 - La cuenta " + accountNumber + " no tiene movimientos")
                let respuesta = {"msg" : "La cuenta no tiene movimientos"};
                res.status(404);
                res.send(respuesta);
              }
            }
            else {
              console.log("getAccountTransactionsV1 - Se ha producido un error accediendo a los movimientos de la cuenta" + accountNumber);
              let respuesta = {"msg" : "No se han podido recuperar los movimientos de la cuenta"};
              res.status(500);
              res.send(respuesta);
            }
          }
        );
      }
      else {
        console.log("getAccountTransactionsV1 - El usuario de la petición no se corresponde con un interviniente de la cuenta");
        let respuesta = {"msg" : "El usuario de la petición no se corresponde con un interviniente de la cuenta"};
        res.status(404);
        res.send(respuesta);
      }
    },function(error){
        console.log("getAccountTransactionsV1 - " + error);
        res.status(error.statusCode);
        res.send(error.respuesta);
    });
  }
  else {
    let respuesta = {"msg" : "Token inválido"};
    res.status(401);
    res.send(respuesta);
  }
}

function createAccountTransactionsV1(req,res){
  var userId = req.params.id1;
  var accountNumber = req.params.id2;
  var transaction_amount = Number(req.body.amount);
  var token = req.headers['authorization'];

  console.log("createAccountTransactionsV1 - POST /apitechu/v1/users/" + userId + "/accounts/" + accountNumber);
  console.log ("createAccountTransactionsV1 - Añadiendo un movimiento a la cuenta " + accountNumber + " del usuario " + userId);
  console.log("createAccountTransactionsV1 - Importe : " + req.body.amount + " " + req.body.currency);

  if(validateToken.validateToken(token)){
    accountController.validateAccount(accountNumber,userId).then(function(account){
      let encontrado = false;
      for (participant of account.participants){
        if (participant.id == userId){
          encontrado = true;
          break;
        }
      }

      if (encontrado){
        console.log("createAccountTransactionsV1 - Se han validado correctamente los datos de la petición de creación del movimiento");

        var newTransaction = {
          "account_number" : accountNumber,
          "type" : req.body.type,
          "amount" : transaction_amount,
          "currency" : req.body.currency,
          "operation_date": new Date(),
          "valuable_date": utilDate.nextValuableDate(),
          "detail" : req.body.detail
        }

        if(req.body.type=="R"){
          transaction_amount = transaction_amount * (-1);
        }

        createTransaction(newTransaction,req.body.account_balance,req.body.account_currency_balance).then(function(response){
          accountController.updateAccountBalance(accountNumber,req.body.account_balance,req.body.account_currency_balance,transaction_amount,req.body.currency).then(function(response){
            console.log("createAccountTransactionsV1 - Se ha actualizado correctamente el saldo de la cuenta " + accountNumber);
            res.status(response.statusCode);
            res.send(response.respuesta)
          },function(error){
            console.log("createAccountTransactionsV1 - " + error);
            res.status(error.statusCode);
            res.send(error.respuesta);
          });
        },function(error){
          console.log("createAccountTransactionsV1 - " + error);
          res.status(error.statusCode);
          res.send(error.respuesta);
        });
      }
      else {
        console.log("createAccountTransactionsV1 - El usuario de la petición no se corresponde con un interviniente de la cuenta");
        let respuesta = {"msg" : "El usuario de la petición no se corresponde con un interviniente de la cuenta"};
        res.status(404);
        res.send(respuesta);
      }
    },function(error){
        console.log("createAccountTransactionsV1 - " + error);
        res.status(error.statusCode);
        res.send(error.respuesta);
    });
  }
  else{
    let respuesta = {"msg" : "Token inválido"};
    res.status(401);
    res.send(respuesta);
  }
}

function createTransaction(newTransaction,account_balance,currency_balance){
    console.log("createTransaction - Creando movimiento y actualizando saldo de la cuenta");

    var promesa = new Promise(function(resolve,reject){
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("createTransaction - Cliente HTTP para mLab creado");
      httpClient.post("transaction?" + mLabAPIKey, newTransaction,
        function (err, resMLab, body){
          if(resMLab.statusCode >= 400){
            console.log ("createTransaction - Error creando el movimiento: " + body.message);
            reject({"respuesta" : {"msg" : "Se ha producido un error el movimiento"},
                    "statusCode" : 500});
          }
          else{
            console.log("createTransaction - Movimiento con id " + body._id.$oid + " creado con éxito");
            resolve({"respuesta" : {"id":body._id.$oid,
                             "msg" : "Se ha creado correctamente el movimiento"},
                      "statusCode" : 201});
          }
        }
      );
    });

    return promesa;
}

module.exports.getAccountTransactionsV1 = getAccountTransactionsV1;
module.exports.createAccountTransactionsV1 = createAccountTransactionsV1;
