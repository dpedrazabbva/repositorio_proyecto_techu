const jwt = require('jsonwebtoken');
const clavePublica = "Clave Pública";

function validateToken(token){
  if(!token){
    return false;
  }

  token = token.replace('Bearer ', '');

    try{
      var resultado = jwt.verify(token, clavePublica);
      console.log("Token validado correctamente");
      return true;
    }
    catch(error){
      console.log("Se ha producido un error validando el token");
      return false;
    }
}

module.exports.validateToken = validateToken;
